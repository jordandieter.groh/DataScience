#Diese Funktion weist einer Liste an der i. Stelle den j. Wert einer anderen
#Liste zu.
def assignment(new_list, i, old_list, j):
    new_list[i] = old_list[j]

#Diese Funktion sortiert eine Liste durch den merge-sort Algorithmus.
def merge_sort(list_sort):
    #Hier wird rekursiv die Funktion wieder ausgeführt, wenn ihre Länge größer
    #als 1 it.
    if (len(list_sort) > 1):
        mid = len(list_sort) // 2
        left = list_sort[:mid]
        right = list_sort[mid:]

        merge_sort(left)
        merge_sort(right)

        l = 0
        r = 0
        i = 0

        #Hier werden die sortierten Teillisten passend zusammengesetzt.
        #Das passiert mithilfe der assignment Funktion.
        while l < len(left) and r < len(right):
            if left[l] <= right[r]:
                assignment(list_sort, i, left, l)
                l += 1
            else:
                assignment(list_sort, i, right, r)
                r += 1
            i += 1

        while l < len(left):
            list_sort[i] = left[l]
            l += 1
            i += 1

        while r < len(right):
            list_sort[i] = right[r]
            r += 1
            i += 1


#Hier beginnt der Test der geschriebenen Funktionen.
import matplotlib.pyplot as plt

my_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]
x = range(len(my_list))
plt.scatter(x, my_list)
plt.title('unsortierte Liste')
plt.xlabel('Index der Liste')
plt.ylabel('Eintrag der Liste')
plt.show()
mergeSort(my_list)
plt.scatter(x, my_list)
plt.title('sortierte Liste')
plt.xlabel('Index der Liste')
plt.ylabel('Eintrag der Liste')
plt.show()
